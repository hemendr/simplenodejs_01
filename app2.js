var http = require('http');
var express = require('express');
var fs = require('fs');

const app = express();
var server_port = process.env.OPENSHIFT_NODEJS_PORT || 8082;
var server_ip_address = process.env.OPENSHIFT_NODEJS_IP || '0.0.0.0';
var TEST_1 = process.env.TEST_1;
var server = null;




app.get('/', function(req, res) 
{
	
	
	var host = server.address().address;
	var port = server.address().port;
	console.log("Got a GET request for the homepage %s %s", host, port);
	res.writeHead(200, {'Content-Type': 'text/plain'});
	res.write('Node JS deployed in the Openshift platoform @2, Hemendr GET');
	res.end();
});

//This responds a POST request for the homepage
app.post('/', function (req, res) {
	
	var host = server.address().address;
	var port = server.address().port;
	
	console.log("Got a POST request for the homepage");
	res.writeHead(200, {'Content-Type': 'text/plain'});
	res.write('Node JS deployed in the Openshift platoform @2, Hemendr POST - Env.TEST_1=%s, host = %s, port =%s', TEST_1, host, port);
	res.end();
});

app.get('/listUsers', function (req, res) {
   fs.readFile( __dirname + "/" + "Users.json", 'utf8', function (err, data) {
       console.log( data );
       res.end( data );
   });
})

function onListen()
{
	var host = server.address().address;
	var port = server.address().port;
	console.log('Server running at http:%s:%s', host, port);

}

server = app.listen(server_port);
console.log('Server running at http://0.0.0.0:'+server_port);

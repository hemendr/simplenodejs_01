FROM node:9.5.0
RUN echo 'Openshift/Kubernetes Proof of Concept'
EXPOSE 8080
RUN npm install
COPY app.js .
CMD ["npm", "start" ]

'use strict';

var express = require('express');
var fs = require('fs');
const app = express();
var server_port = process.env.NODEJS_PORT || 8080;
var server_ip_address = process.env.NODEJS_IP || '0.0.0.0';

var server = null;


app.get('/symUsers', function (req, res) {
   fs.readFile( __dirname + "/" + "Users.json", 'utf8', function (err, data) {
       console.log( data );
       res.end( data );
   });
})

function onListen()
{
	var host = server.address().address;
	var port = server.address().port;
	console.log('Server running at http:%s:%s', host, port);
}

server = app.listen(server_port);
console.log('Server running at http://0.0.0.0:'+server_port);
